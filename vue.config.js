module.exports = {
  lintOnSave: false
}
const path = require("path");
const resolve = (dir) => path.join(__dirname, dir);
module.exports = {
  chainWebpack(config) {
    config.resolve.alias
      .set("vue$", "vue/dist/vue.esm.js")
      .set("@common", resolve("src/common"))
      .set("@dmp", resolve("src/application/dmp/src"))
      .set("@dcs", resolve("src/application/dcs/src"))
  },
};
