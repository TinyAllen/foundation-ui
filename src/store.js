import Vue from 'vue'
import Vuex from 'vuex'
import dmp from './application/dmp/src/store'
import dcs from './application/dcs/src/store'
Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    menu: [
      ...dmp.state.menu,
      ...dcs.state.menu,
    ]
  },
  mutations: {

  },
  actions: {

  }
})
