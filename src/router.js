import Vue from 'vue'
import Router from 'vue-router'
import dmp from './application/dmp/src/router'
import dcs from './application/dcs/src/router'
Vue.use(Router)

export default new Router({
  mode: 'history',
  linkActiveClass: 'active',
  routes: [
   ...dmp,
   ...dcs,
  ]
})
